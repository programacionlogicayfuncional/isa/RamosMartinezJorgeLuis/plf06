(ns plf06.core
  (:gen-class))


(let [alfa [\a \á \b \c \d \e \é \f \g \h \i \í \j \k \l \m \n \ñ \o \ó \p \q \r \s \t \u \ú \ü \v \w \x \y \z]
      caracter [\0 \1 \2 \3 \4 \! \" \# \$ \% \& \' \( \) \* \+ \, \- \. \/ \: \; \< \= \> \? \@ \[ \\ \] \^ \_ \` \{ \| \} \~ \5 \6 \7 \8 \9]]
  (defn cifrar
    [c]
    (cond
      (Character/isUpperCase c) (Character/toUpperCase (nth (drop (.indexOf alfa (Character/toLowerCase c)) (take (* (count alfa) 2) (cycle alfa))) 13))
      (Character/isLowerCase c) (nth (drop (.indexOf alfa c) (take (* (count alfa) 2) (cycle alfa))) 13)
      (not= -1 (.indexOf caracter c)) (nth (drop (.indexOf caracter c) (take (* (count caracter) 2) (cycle caracter))) 13)
      :else c))
  (defn rot-13
    [text]
    (apply str (map cifrar (map char text)))))

(defn -main
  [& args]
  (if (empty? args)
    (println "Error! No hay datos que cifrar")
    (println (rot-13 (apply str args)))))
